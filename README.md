# Applications Web Outdoorvision Mer et Weather Data

## Branches

### Principales

- **main** : Fonctionnalité de capture d'éléments cartographiques.

    **URL** : https://application-gwendalleguellec-2b4db33aef167bb37381febbcc9c57565a.gitlab.io/
    
    - Capture de points en cliquant sur les cellules
    - Filtre des points (bateaux) par pays
    - Union/intersection de 2 listes (pas entièrement fonctionnel)
    - Export en CSV du résultat
    - Ajout de tag aux cellules hexagonales via un clic droit (la grille H3 et les points doivent être affichés)
    - Export en CSV de la zone sélectionnée avec les tags

    <img src="images/image1.png" alt="Capture d'éléments cartographiques" width="50%"/>


- **Outdoorvision-final-version** : Contient la version finale de l'application web OVM (visualisation de données d'activités sportives).

    <img src="images/image2.png" alt="Outdoorvision" width="50%"/>

- **Weather-MF** : Contient la version en cours de développement de l'application web (visualisation de données Météo France).
    
    <img src="images/image3.png" alt="Weather Data" width="50%"/>

### Développement

- **keypad** : Contient une ébauche d'un keypad pour entrer des coordonnées H3.

## Diagrammes UML

### Diagramme de l'application
<img src="images/src_diagram.png" alt="Diagramme UML" width="100%"/>

### Diagramme du dossier `/src/models/features/`
<img src="images/features_diagram.png" alt="Diagramme features" width="100%"/>

### Diagramme du dossier `/src/models/grids/`
<img src="images/grids_diagram.png" alt="Diagramme grids" width="100%"/>

## Available Scripts

In the project directory, you can run:

`npm install`

To install dependencies in `nodes_modules`.

`npm start`

To run the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

`npm run build`

To build the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

When the build folder is ready to be deployed.
You may serve it with a static server:

`serve -s build`