import { CSVPointsData } from '../types/DataFetcherTypes';

/**
 * DataFetcher is a class (a service) that fetches data from a CSV file parsing it into object.
 */
export class DataFetcher {
  /**
   * Fetches the lines from a CSV file
   * @param csvFileName CSV file name
   * @returns a list of lines from the CSV file
   */
  private static async getLines(csvFileName: string): Promise<string[]> {
    const response = await fetch(csvFileName);
    const text = (await response.text()).trim();
    const lines = text.split('\n');
    return lines;
  }
  
  /**
   * Fetches data from a CSV file and returns it as a CSVPointsData object
   * @param csvFileName CSV file name
   * @returns a CSVPointsData object
   * @async
   */
  public static async getPointsCSVData(csvFileName: string): Promise<CSVPointsData> {
    const lines = await this.getLines(csvFileName);
    const data: CSVPointsData["data"] = [];

    for (let i = 1; i < lines.length; i++) {
      const parts = lines[i].split(',')
      const timestamp = parts[0];
      const lon = parseFloat(parts[1]);
      const lat = parseFloat(parts[2]);
      const hdms = parts[3];
      const mmsi = parts[4];
      const imo = parts[5];
      const callsign = parts[6];
      const name = parts[7];
      const flag = parts[8];
      const cargo = parts[9];
      const type = parts[10];
      const cog = parseFloat(parts[11]);
      const hdg = parseFloat(parts[12]);
      const sog = parseFloat(parts[13]);
      const length = parseFloat(parts[14]);
      const width = parseFloat(parts[15]);
      const draught = parseFloat(parts[16]);
      const destination = parts[17];
      const departure = parts[18];
      const eta = parts[19];

      data.push({
        timestamp, lon, lat, hdms, mmsi, imo, callsign, name, flag, cargo, type, cog, hdg, sog, length, width, draught, destination, departure, eta
      });
    }

    return { data };
  }
}