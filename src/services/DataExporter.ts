import { PointFeature } from "../models/features/PointFeature";

/**
 * DataExporter is a class (a service) that exports data to a CSV file.
 */
export class DataExporter {

    /**
     * Returns a CSV string with the necessary data for each point.
     * @param data list of points to be written in the CSV file
     * @returns a CSV string with the necessary data for each point
     */
    private static parsePointData(data: PointFeature[]): string {
        let csv: string = "";
        data.forEach(point => {
            const data = point.get('data');
            csv += `${data.flag},${data.timestamp},${data.mmsi},${data.name},${data.type}\n`;
        });
        return csv;
    }

    private static parseHexagonsData(data: string[]): string {
        let csv: string = "";
        data.forEach(line => {
            csv += `${line}\n`;
        });
        return csv;
    }

    /**
     * Creates a CSV file with the given header, data and file name.
     * @param header first line of the CSV file with the columns names
     * @param data list of data to be written in the CSV file
     * @param fileName name of the CSV file
     */
    public static createCSVFile(header: string, data: string[] | PointFeature[], fileName: string): void {
        // Start the CSV file with the header
        let csv = header + '\n';
        
        // Parse the data
        if (data[0] instanceof PointFeature) {
            csv += this.parsePointData(data as PointFeature[]);
        } else {
            csv += this.parseHexagonsData(data as string[]);
        }

        // Create and download the CSV file
        const blob = new Blob([csv], { type: 'text/csv' });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.setAttribute('href', url);
        a.setAttribute('download', fileName);
        a.click();
    }
}