import * as ReactDOMClient from 'react-dom/client';
import './styles/index.css';
import App from './components/App';
import React from 'react';

const container = document.getElementById('root') as HTMLElement;

// Create a root - https://github.com/reactwg/react-18/discussions/5
const root = ReactDOMClient.createRoot(container);

// Initial render: Render an element to the root
root.render(
  <App />
);