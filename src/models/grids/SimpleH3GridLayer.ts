import { H3GridLayer } from './H3GridLayer';
import { HexagonFeature } from '../features/HexagonFeature';
import { getH3Resolution } from '../h3/H3Utilities';
import { polygonToCells } from 'h3-js';
import { Extent } from 'ol/extent';

export class SimpleH3GridLayer extends H3GridLayer {

    /**
     * Create a list of HexagonFeature from a polygon and a resolution.
     * @param Extent the extent to cover with hexagons
     * @returns a list of HexagonFeature
     */
    override createFeatures(extent: Extent): HexagonFeature[] {
        const polygon = this.getPolygon(extent);
        const refResolution = getH3Resolution(this.map);

        let features: HexagonFeature[] = [] as HexagonFeature[];
        try {
            features = HexagonFeature.createHexagonFeatures(polygonToCells(polygon, refResolution), true);
        } catch (error) {
            console.log("SimpleH3GridLayer - refreshGrid - createFeatures : H3Error - input is invalid or output is too large for JS");
        }
        
        return features;
    }
}