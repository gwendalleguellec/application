import { SquareFeature } from '../features/SquareFeature';
import { GridLayer } from './GridLayer';
import { Extent } from 'ol/extent';

/**
 * SquareGridLayer is a GridLayer that displays a grid of squares.
 */
export class SquareGridLayer extends GridLayer {

    /**
     * Create a list of SquareFeature from a polygon.
     * @param polygon the polygon to cover (the polygon is a list of coordinates)
     * @param zoom the zoom level of the map
     * @returns a list of SquareFeature
     */
    override createFeatures(extent: Extent): SquareFeature[] {
        const polygon = this.getPolygon(extent).map((coord) => [coord[0], coord[1]]);
        polygon.forEach((coords) => {
            coords.reverse();
        });
        const refResolution = this.map.getView().getZoom() as number;
        
        const features = SquareFeature.createSquareFeatures(polygon, refResolution);
        return features;
    }
}