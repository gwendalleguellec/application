import { GridLayer } from './GridLayer';
import { HexagonFeature } from '../features/HexagonFeature';

import { polygonToCells } from 'h3-js';
import { getH3Resolution } from '../h3/H3Utilities';
import { Extent } from 'ol/extent';

/**
 * H3GridLayer is a GridLayer that displays a grid of H3 hexagons.
 */
export class H3GridLayer extends GridLayer {

    /**
     * Create a list of HexagonFeature from a polygon and a resolution.
     * @param Extent the extent to cover with hexagons
     * @returns a list of HexagonFeature
     */
    override createFeatures(extent: Extent): HexagonFeature[] {
        const polygon = this.getPolygon(extent);
        const refResolution = getH3Resolution(this.map);
        let features = [] as HexagonFeature[];

        try {            
            features = [
                HexagonFeature.createHexagonFeatures(polygonToCells(polygon, refResolution), true),
                HexagonFeature.createHexagonFeatures(polygonToCells(polygon, refResolution + 1), true),
                HexagonFeature.createHexagonFeatures(polygonToCells(polygon, refResolution - 1), true)
            ].flat();
        } catch (error) {
            console.log("H3GridLayer - refreshGrid - createFeatures : H3Error - input is invalid or output is too large for JS");
        }

        return features;
    }
}