import { Group as LayerGroup } from 'ol/layer';
import { Vector as VectorSource } from 'ol/source';
import { Feature, Map } from 'ol';
import { Extent } from 'ol/extent';
import { toLonLat } from 'ol/proj';
import VectorImageLayer from 'ol/layer/VectorImage';
import Collection from 'ol/Collection';
import { Geometry } from 'ol/geom';

/**
 * GridLayer is a VectorImageLayer that displays a grid of squares.
 */
export abstract class GridLayer extends VectorImageLayer<VectorSource<Feature>> {

    protected map: Map;
    protected source: VectorSource<Feature>;

    /**
     * Create a new GridLayer.
     * @param options the options of the layer
     * @param map the map to add the layer to
     */
    constructor(options: any, map: Map) {
        super(options);
        this.source = new VectorSource();
        this.setSource(this.source);
        this.map = map;

        this.map.getView().on('change:center', () => { this.refreshGrid() });
        this.map.getView().on('change:resolution', () => { this.refreshGrid() });
        this.map.on('change:size', () => { this.refreshGrid() });
    }

    /**
     * Refresh the grid displayed on the map.
     */
    public refreshGrid() {
        this.source.clear();
        const viewStateAndExtent = this.map.getView().getViewStateAndExtent();
        const extent = this.scale(viewStateAndExtent.extent, 1.1);
        const features = this.createFeatures(extent);
        this.source.addFeatures(features);
    }

    /**
     * Scale an extent by a factor.
     * @param extent the extent to scale
     * @param factor the factor to scale the extent by
     * @returns the scaled extent
     */
    protected scale(extent: Extent, factor: number): Extent {
        const minX = extent[0], minY = extent[1], maxX = extent[2], maxY = extent[3];
        const width = maxX - minX;
        const height = maxY - minY;
        extent[0] = minX - width * (factor - 1);
        extent[1] = minY - height * (factor - 1);
        extent[2] = maxX + width * (factor - 1);
        extent[3] = maxY + height * (factor - 1);
        return extent;
    }

    /**
    * Get a polygon from an extent.
    * @param extent the extent to get the polygon from
    * @returns the polygon
    */
    protected getPolygon(extent: Extent): number[][] {
        const bottomLeft = toLonLat([extent[0], extent[1]]).reverse();
        const topLeft = toLonLat([extent[0], extent[3]]).reverse();
        const topRight = toLonLat([extent[2], extent[3]]).reverse();
        const bottomRight = toLonLat([extent[2], extent[1]]).reverse();
        return [bottomLeft, topLeft, topRight, bottomRight, bottomLeft];
    }

    /**
     * Create a list of features to display on the map.
     * This method should be implemented by subclasses.
     * @param Extent the extent to cover with features
     * @returns a list of features
     */
    abstract createFeatures(Extent: Extent): Feature[];

    /**
     * Add a layer to a layer group.
     * @param layerGroup the layer group to add the layers to
     * @param layers the layers to add to the layer group
     * @returns the layer group with the layers added
     */
    public static setLayersToLayerGroup(layerGroup: LayerGroup, layers: { [key: string]: Feature<Geometry>[] }) {
        const layersToAdd = Object.keys(layers).map((layerName) => {
            const newLayer = new VectorImageLayer({
                source: new VectorSource({
                    features: layers[layerName],
                }),
            });
            return newLayer;
        });

        layerGroup.setLayers(new Collection(layersToAdd));
    };

    /**
     * Add a layer to a layer group.
     * @param layerGroup the layer group to add the layer to
     * @param layer the layer to add to the layer group
     */
    public static addLayerToLayerGroup(layerGroup: LayerGroup, layer: Feature<Geometry>[]) {
        const newLayer = new VectorImageLayer({
            source: new VectorSource({
                features: layer,
            }),
        });
        layerGroup.getLayers().push(newLayer);
    }
}