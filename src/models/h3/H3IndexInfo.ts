//
// H3Index Representation
//
//          0x0F        0x0E	    0x0D	    0x0C	    0x0B	    0x0A	    0x09	    0x08	    0x07	    0x06	    0x05    	0x04	    0x03	    0x02	    0x01	    0x00
// 0x30	    	          Mode	                                        Res/edge	                        Resolution	                                    Base cell
// 0x20	    Base cell	                        Digit 1	                            Digit 2	                            Digit 3	                            Digit 4	                            Digit 5
// 0x10	    Digit 5	                Digit 6	                            Digit 7	                            Digit 8	                            Digit 9	                            Digit 10
// 0x00	    Digit 10    Digit 11	                        Digit 12	                        Digit 13	            Digit 14	                        Digit 15
//
export class H3IndexInfo {
    constructor(str: string) {
        if (str != null) {
            if (str.length === 15) {
                this.bits += "0000";
            }
            for (let i = 0; i < str.length; i++) {
                let c = str[i];
                this.bits += this.hexCode[c];
            }
            //this.bits = this.reverse( this.bits );
        }
    }

    public getBits(): string {
        return this.bits;
    }

    public getLiteral(): number[] {
        let cellNbs: number[] = [];
        cellNbs.push(this.getFaceNb())
        for (let i = 1; i <= 15; i++) {
            const cellNb = this.getCellNb(i)
            if (cellNb !== 7) {
                cellNbs.push(cellNb)
            }
            else {
                break;
            }
        }
        return cellNbs;
    }

    getAugmentedLiteral(): string {
        let str: string = '' + this.getFaceNb();
        str += '-';
        const res1 = this.getCellNb(1);
        if (res1 !== 7) {
            str += res1;
            const res2 = this.getCellNb(2);
            if (res2 !== 7) {
                str += res2;
                const res3 = this.getCellNb(3);
                if (res3 !== 7) {
                    str += '-' + res3;
                    const res4 = this.getCellNb(4);
                    if (res4 !== 7) {
                        str += res4;
                        const res5 = this.getCellNb(5);
                        if (res5 !== 7) {
                            str += res5;
                            const res6 = this.getCellNb(6);
                            if (res6 !== 7) {
                                str += '-' + res6;
                                const res7 = this.getCellNb(7);
                                if (res7 !== 7) {
                                    str += res7;
                                    const res8 = this.getCellNb(8);
                                    if (res8 !== 7) {
                                        str += res8;
                                        const res9 = this.getCellNb(9);
                                        if (res9 !== 7) {
                                            str += '-' + res9;
                                            const res10 = this.getCellNb(10);
                                            if (res10 !== 7) {
                                                str += res10;
                                                const res11 = this.getCellNb(11);
                                                if (res11 !== 7) {
                                                    str += res11;
                                                    const res12 = this.getCellNb(12);
                                                    if (res12 !== 7) {
                                                        str += '-' + res12;
                                                        const res13 = this.getCellNb(13);
                                                        if (res13 !== 7) {
                                                            str += res13
                                                            const res14 = this.getCellNb(14);
                                                            if (res14 !== 7) {
                                                                str += res14;
                                                                const res15 = this.getCellNb(15);
                                                                if (res15 !== 7) {
                                                                    str += res15;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return str;
    }

    public getFaceNb(): number {
        return this.getInt(12, 7);
    }

    public getDirection(): number {
        return this.getInt(5, 3);
    }

    // Can return Nan if empty index
    public getCellNb(resolution: number): number {
        let offset = 16 + 3 * resolution;
        return this.getInt(offset, 3);
    }
    
    /**
     * Transform H3IndexInfo in bits to integer format
     * Use the website https://h3geo.org/docs/core-library/h3Indexing to understand all bits signification.
     * @param offset use to select the resolution
     * @param length 
     * @returns an integer 
     */
    public getInt(offset: number, length: number) {
        let s = "";
        for (let i = offset; i < offset + length; i++) {
            s = this.bits[i] + s;
        }
        s = this.reverse(s);
        let result = parseInt(s, 2);
        return result;
    }

    private reverse(str: string) {
        return str.split("").reverse().join("");
    }

    private hexCode: { [key: string]: string } = {
        '0': "0000",
        '1': "0001",
        '2': "0010",
        '3': "0011",
        '4': "0100",
        '5': "0101",
        '6': "0110",
        '7': "0111",
        '8': "1000",
        '9': "1001",
        'a': "1010",
        'b': "1011",
        'c': "1100",
        'd': "1101",
        'e': "1110",
        'f': "1111"
    };

    bits: string = "";
}

