import { Map } from 'ol';
import { Zoom2Resolution } from '../../types/H3UtilitiesTypes';

export function getH3Resolution(map: Map): number {
    let zoom = map.getView().getZoom();
    if (zoom) {
        zoom = Math.floor(zoom);
        return zoomLevelToHexagonsResolution[zoom];
    }
    // Arbitrary large cells to avoid drawing far too many cells
    return 3;
}

export const zoomLevelToHexagonsResolution: Zoom2Resolution = {
    // 1: 1,
    2: 0,
    3: 0,
    4: 1,
    5: 2,
    6: 2,
    7: 3,
    8: 4,
    9: 4,
    10: 5,
    11: 6,
    12: 7,
    13: 7,
    14: 8,
    15: 9,
    16: 9,
    17: 10,
    18: 10,
    19: 11,
    20: 12,
    21: 12
}

/**
 * Convert a number id (0-122) to a letter id (AA-AZ, BA-BZ, ..., EA-ER)
 * @param numberId 
 * @returns 
 */
export function convertNumberIdToLettersId(numberId: number): string {
    let letterId = '';
    if (numberId >= 0 && numberId <= 122) {
        const firstLetter = String.fromCharCode(Math.floor(numberId / 26) + 65);
        const secondLetter = String.fromCharCode(numberId % 26 + 65);
        letterId = firstLetter + secondLetter;
    }
    return letterId;
}

/**
 * Convert a letter id (AA-AZ, BA-BZ, ..., EA-ER) to a number id (0-122)
 */
export function convertLettersIdToNumberId(letterId: string): number {
    let numberId = 0;
    if (letterId.length === 2) {
        numberId = (letterId.charCodeAt(0) - 65) * 26 + letterId.charCodeAt(1) - 65;
    }
    return numberId;
}