import { latLngToCell } from 'h3-js';
import { H3IndexInfo } from './H3IndexInfo';
import { H3IndexBuilder } from './H3IndexBuilder';

export class H3IndexBuilderTester {
    // refH3Index
    //   Mode     Res  Base    1   2   3   4  5   6   7   8  9   10  11  12 13  14  15   
    // "0000 1000 0101 0001 1000 0100 0100 0010 0011 1111 1111 1111 1111 1111 1111 1111"
    //      "8    5    1    8    4    4    2    3    f    f    f    f    f    f    f"
    // builder
    //                           4    4    2    12-10410
    // 12-10410

    //      "1000 0101 0001 1000 0100 0100 0010 0011 1111 1111 1111 1111 1111 1111 1111"
    public static check() {
        const refH3Index = latLngToCell(47.570869, -2.874568, 5);
        const refH3IndexInfo = new H3IndexInfo(refH3Index);
        console.log(refH3IndexInfo);
        // const exploded = refH3IndexInfo.getFaceNb() + "-" +
        //     refH3IndexInfo.getCellNb(1) +
        //     refH3IndexInfo.getCellNb(2) + "-" +
        //     refH3IndexInfo.getCellNb(3) +
        //     refH3IndexInfo.getCellNb(4) +
        //     refH3IndexInfo.getCellNb(5);

        // const refBits = refH3IndexInfo.getBits();

        const h3ib = new H3IndexBuilder();
        h3ib.setBaseCell(12);
        h3ib.setDigitAtResolution(1, 1);
        h3ib.setDigitAtResolution(0, 2);
        h3ib.setDigitAtResolution(4, 3);
        h3ib.setDigitAtResolution(1, 4);
        h3ib.setDigitAtResolution(0, 5);

        const h3Index = h3ib.get();

        // const bits = h3ib.getBits();

        const h3IndexInfo = new H3IndexInfo(h3Index);
        const digit5 = h3IndexInfo.getCellNb(5);
        console.log(digit5);
    }
}