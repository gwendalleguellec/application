export class H3IndexBuilder {
    private maxResolution: number = 0
    // private START_BLOCK_LENGTH = 4;

    private bitValue: { [digit: number]: string } = {
        0: "000",
        1: "001",
        2: "010",
        3: "011",
        4: "100",
        5: "101",
        6: "110",
        7: "111",
    }

    private hexCode: { [key: string]: string } = {
        "0000": "0",
        "0001": "1",
        "0010": "2",
        "0011": "3",
        "0100": "4",
        "0101": "5",
        "0110": "6",
        "0111": "7",
        "1000": "8",
        "1001": "9",
        "1010": "a",
        "1011": "b",
        "1100": "c",
        "1101": "d",
        "1110": "e",
        "1111": "f"
    }

    private bits: string = "";

    constructor() {
        this.initBits();
    }

    public get(): string {
        this.setModeToCell();
        this.setResolution();
        this.setUndefinedResolutions();
        return this.toHexCode();
    }

    public getBits(): string {
        return this.bits;
    }

    private setModeToCell(): void {
        const strBits = Array.from(this.bits);
        strBits[4] = '1';
        this.bits = strBits.join('');
    }

    private setResolution(): void {
        const START_RESOLUTION = 8
        const toBits = this.maxResolution.toString(2).padStart(4, "0");
        const strBits = Array.from(this.bits);
        for (let i = 0; i < 4; i++) {
            strBits[START_RESOLUTION + i] = toBits[i];
        }
        this.bits = strBits.join('');
    }

    public setBaseCell(faceNb: number): void {
        const START_BASE_CELL = 12;
        const toBits = faceNb.toString(2).padStart(7, "0");
        const strBits = Array.from(this.bits);
        for (let i = 0; i < 7; i++) {
            strBits[START_BASE_CELL + i] = toBits[i];
        }
        this.bits = strBits.join('');
    }

    public setDigitAtResolution(digit: number, resolution: number): void {
        if (resolution > this.maxResolution) {
            this.maxResolution = resolution;
        }
        this.setDigitAtResolutionInternal(digit, resolution);
    }

    private setDigitAtResolutionInternal(digit: number, resolution: number): void {
        const offset = 16 + 3 * resolution;
        this.set(offset, 3, digit);
    }

    private setUndefinedResolutions(): void {
        const strBits = Array.from(this.bits);
        for (let i = this.maxResolution + 1; i < 16; i++) {
            const offset = 16 + 3 * i;
            for (let j = offset; j < offset + 3; j++) {
                strBits[j] = "1";
            }
        }
        this.bits = strBits.join('');
    }

    private set(offset: number, length: number, value: number): void {
        const strBits = Array.from(this.bits);
        const bitValue = this.bitValue[value];
        for (let i = 0; i < length; i++) {
            strBits[i + offset] = bitValue[i];
        }
        this.bits = strBits.join('');
    }

    private initBits(): void {
        for (let i = 0; i < 16; i++) {
            this.bits += "0000";
        }
    }

    private toHexCode(): string {
        let hex: string = ""
        this.bits = this.bits.substring(4);
        for (let i = 0; i < 60; i += 4) {
            const s = this.bits.substring(i, i + 4);
            hex += this.hexCode[s];
        }
        return hex
    }
}