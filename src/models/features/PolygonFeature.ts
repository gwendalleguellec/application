import Feature from 'ol/Feature';
import { Polygon, Geometry } from 'ol/geom';
import { PointFeature } from './PointFeature';
import { SquareFeature } from './SquareFeature';

import { Coordinate } from 'ol/coordinate';
import { transform } from 'ol/proj';
import { EPSG_3857, EPSG_4326 } from '../EPSG';
import { cellToParent, getResolution } from 'h3-js';

export abstract class PolygonFeature extends Feature<Polygon> {

    protected coords: Coordinate[];

    constructor(coords: Coordinate[]) {
        super();

        this.coords = coords;
        this.setGeometry(new Polygon([coords.map((coord) => transform(coord, EPSG_4326, EPSG_3857))]));
    }

    /**
     * Get the coordinates of the square.
     * @returns the coordinates of the square
     */
    protected getCoords(): Coordinate[] {
        return this.coords;
    }

    /**
     * Get the points in the polygon.
     * @param polygon the polygon
     * @param pointsList the list of points
     * @returns the points in the polygon
     */
    protected static getPointsInPolygonFeature(polygon: SquareFeature | string, pointsList: PointFeature[]) {
        const points: PointFeature[] = [];

        if (typeof polygon === 'string') {
            const hexagonResolution = getResolution(polygon);

            pointsList.forEach((point: PointFeature) => {
                const pointH3Index = point.get('h3');
                const hexagonH3IndexResolution = cellToParent(pointH3Index, hexagonResolution);
    
                if (hexagonH3IndexResolution === polygon) {
                    point.setCaughtPointStyle();
                    points.push(point);
                }
            });
        } else {
            const squareGeometry = polygon.getGeometry() as Geometry;
            
            pointsList.forEach((point) => {
                const pointGeometry = point.getGeometry() as Geometry;
                if (squareGeometry.intersectsExtent(pointGeometry.getExtent())) {
                    point.setCaughtPointStyle();
                    points.push(point);
                }
            });
        }
            
        return points;
    }

    protected abstract setDefaultPolygonStyle(): void;
    public abstract setCaughtPolygonStyle(): void;
}