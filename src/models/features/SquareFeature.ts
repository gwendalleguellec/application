import { Style, Stroke, Fill } from 'ol/style';
import { Coordinate } from 'ol/coordinate';
import { PointFeature } from './PointFeature';
import { PolygonFeature } from './PolygonFeature';

/**
 * SquareFeature is a Feature that displays a square.
 */
export class SquareFeature extends PolygonFeature {

    private squareSize: number;

    /**
     * Create a new SquareFeature.
     * @param coords the coordinates of the square
     * @param zoom the zoom level of the map
     */
    constructor(coords: Coordinate[], squareSize: number) {
        super(coords);

        this.squareSize = squareSize;
        this.setDefaultPolygonStyle();
    }
    
    // ========================================= STYLING =================================================

    /**
     * Set the style of the square depending on its size (depending on the zoom level of the map)
     */
    override setDefaultPolygonStyle() {
        const exposant = Math.log2(this.getSquareSize());

        if (exposant % 2 === 0) {
            this.setStyle(new Style({
                stroke: new Stroke({
                    color: 'red',
                    width: 0.5,
                }),
            }));
        } else {
            this.setStyle(new Style({
                stroke: new Stroke({
                    color: 'blue',
                    width: 0.5,
                }),
            }));
        }
    }

    /**
     * Set the style of the square when it is caught.
     */
    override setCaughtPolygonStyle() {
        this.setStyle(new Style({
            fill: new Fill({
                color: 'rgba(255, 100, 255, 0.3)',
            }),
            stroke: new Stroke({
                color: 'violet',
                width: 1,
            }),
        }));
    }

    // ========================================= UTILITIES ===============================================

    /**
     * Check if the square has the same coordinates as another square.
     * @param square the square to compare
     * @returns true if the squares have the same coordinates, false otherwise
     */
    public hasSameCoordinates(square: SquareFeature): boolean {
        return square.getCoords().toString() === this.coords.toString();
    }

    // ========================================= GETTERS & SETTERS ======================================

    /**
     * Get the size of the square.
     * @returns the size of the square
     */
    private getSquareSize(): number {
        return this.squareSize;
    }

    // ========================================= STATIC METHODS =========================================

    /**
     * Create a list of SquareFeature from a polygon.
     * @param polygon the polygon to cover (the polygon is a list of coordinates)
     * @param zoom the zoom level of the map
     * @returns a list of SquareFeature
     */
    public static createSquareFeatures(polygon: number[][], zoomFloat: number): SquareFeature[] {
        const features: SquareFeature[] = [];
        const squareSize = this.calculateSquareSize(zoomFloat);

        for (let i = polygon[0][0]; i < polygon[2][0]; i += squareSize) {
            for (let j = polygon[0][1]; j < polygon[2][1]; j += squareSize) {
                const coords = SquareFeature.calculateSquareCoordinates(i, j, squareSize);
                const square = new SquareFeature(coords, squareSize);
                features.push(square);
            }
        }

        return features;
    }

    /**
     * Calculate the size of a square at a given zoom level.
     * @param zoom the zoom level
     * @returns the size of a square
     */
    private static calculateSquareSize(zoom: number): number {
        return 1 / Math.pow(2, Math.round(zoom) - 7);
    }

    /**
     * Calculate the coordinates of a square (the grid is aligned on even coordinates to avoid offsets).
     * @param longitude the longitude
     * @param latitude the latitude
     * @param squareSize the size of the square 
     * @returns the coordinates of the square
     */
    private static calculateSquareCoordinates(longitude: number, latitude: number, squareSize: number): Coordinate[] {
        latitude = Math.round(latitude / squareSize) * squareSize;
        longitude = Math.round(longitude / squareSize) * squareSize;

        return [
            [longitude, latitude],
            [longitude + squareSize, latitude],
            [longitude + squareSize, latitude + squareSize],
            [longitude, latitude + squareSize],
            [longitude, latitude],
        ];
    }

    /**
     * Get the points in a list of squares.
     * @param squareList the list of squares
     * @param pointsList the list of points
     * @returns the points in the list of squares
     */
    public static getPointsInListOfSquares(squareList: SquareFeature[], pointsList: PointFeature[]) {
        const points: PointFeature[] = [];

        squareList.forEach(square => {
            const tmp = SquareFeature.getPointsInPolygonFeature(square, pointsList);
            tmp.forEach(point => {
                if (!points.includes(point)) {
                    points.push(point);
                }
            });
        });

        return points;
    }
}