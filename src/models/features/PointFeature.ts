import { Feature } from 'ol';
import { Geometry, Point } from 'ol/geom';
import { fromLonLat } from 'ol/proj';
import { latLngToCell } from 'h3-js';
import { Style, Fill, Stroke } from "ol/style";
import CircleStyle from "ol/style/Circle";

export class PointFeature extends Feature<Point> {

    /**
     * Create a new PointFeature from a record
     * @param record the record to create the PointFeature from
     */
    constructor(record: any) {
        super({
            geometry: new Point(fromLonLat([record.lon, record.lat])),
            h3: latLngToCell(record.lat, record.lon, 11),
            data: record,
        });
    }

    /**
     * Set the style of the point when it is caught
     */
    public setCaughtPointStyle(): void {
        this.setStyle(new Style({
            image: new CircleStyle({
                radius: 5,
                fill: new Fill({ color: 'red' }),
                stroke: new Stroke({ color: 'black', width: 1 }),
            }),
        }));
    }

    /**
     * Create point features from data
     * @param data the data to create the point features from
     * @returns the list of point features
     */
    public static createPointFeatures(data: any[]): PointFeature[] {
        const features = data.map((record: any) => {
            return new PointFeature(record);
        });
        return features;
    }

    /**
     * Show the points in the hexagon
     * @param list the list of points to show in the menu
     */
    public static listPointsInTheMenu(listPoints: Feature<Geometry>[], ulHTMLId: string): void {
        const ul = document.getElementById(ulHTMLId) as HTMLElement;
        if (listPoints.length > 0) {
            ul.innerHTML = listPoints.map((point) => {
                return '' +
                    '<li id="listPointsLi">' +
                    '   <div>' +
                    '       <div id="flagIcon">' + PointFeature.country2flag(point.get('data').flag) + '</div>' +
                    '       <p id="countryCode">' + point.get('data').flag.toUpperCase() + '</p>' +
                    '   </div>' +
                    '   <div>' +
                    '       <div>' + point.get('data').timestamp.replace('T', ' ') + '</div>' +
                    '       <div>' + point.get('data').mmsi + ' ' + point.get('data').name + '</div>' +
                    '       <div>' + point.get('data').type + '</div>' +
                    '   </div>' +
                    '</li>';
            })
            .join('<br>');
        } else {
            ul.innerHTML = '';
        }
    }

    public static country2flag(countryCode: string): string {
        return countryCode
            .toUpperCase()
            .split('')
            .map(char => String.fromCodePoint(char.charCodeAt(0) + 0x1F1A5))
            .join('');
    }
}