import { PolygonFeature } from './PolygonFeature';
import { PointFeature } from './PointFeature';

import { Coordinate } from 'ol/coordinate';
import { Style, Stroke, Text, Fill } from 'ol/style';

import { cellToBoundary, getResolution } from 'h3-js';
import { H3IndexInfo } from '../h3/H3IndexInfo';
import { convertNumberIdToLettersId } from '../h3/H3Utilities';

/**
 * HexagonFeature is a Feature that displays an H3 hexagon.
 */
export class HexagonFeature extends PolygonFeature {
    private h3Cell: string;
    private hexagonTag: string;

    constructor(h3Cell: string) {
        let coords = cellToBoundary(h3Cell) as Coordinate[];
        coords = coords.map((coord) => [coord[1], coord[0]]); // reverse coords
        
        super(coords);

        this.h3Cell = h3Cell;
        this.hexagonTag = '';

        this.setDefaultPolygonStyle();
    }

    // ========================================= STYLING =================================================

    /**
     * Set the style of the hexagon depending on its resolution
     */
    override setDefaultPolygonStyle() {
        if (getResolution(this.h3Cell) === 5) { // Résolution 5 : couleur rose en gras
            this.setStyle(new Style({
                stroke: new Stroke({
                    color: 'rgb(255, 132, 192, 0.2)',
                    width: 8,
                }),
            }));
        }
        else if (getResolution(this.h3Cell) % 2 === 0) { // Résolution paire : couleur verte
            this.setStyle(new Style({
                stroke: new Stroke({
                    color: 'rgba(0, 100, 0, 0.1)',
                    width: 2,
                }),
            }))
        }
        else {
            this.setStyle(new Style({ // Résolution impaire : couleur bleue
                stroke: new Stroke({
                    color: 'rgba(0, 0, 100, 0.2)',
                    width: 2,
                }),
            }))
        }
    }

    /**
     * Set the style for a caught hexagon
     * @returns a HexagonFeature with the style set
     */
    override setCaughtPolygonStyle() {
        const h3IndexInfo = new H3IndexInfo(this.h3Cell);
        const literal = convertNumberIdToLettersId(Number(h3IndexInfo.getAugmentedLiteral().split('-')[0])) + '-' + h3IndexInfo.getAugmentedLiteral().split('-').slice(1).join('-');
        
        this.setStyle(new Style({
            text: new Text({
                text: [
                    this.h3Cell + ' ' + literal, '12px sans-serif',
                    '\n', 'bold 18px sans-serif',
                    this.hexagonTag, 'bold 18px sans-serif'
                ],
            }),
            fill: new Fill({
                color: 'rgba(255, 100, 255, 0.3)',
            }),
            stroke: new Stroke({
                color: 'violet',
                width: 1,
            }),
        }));
    }

    /**
     * Set the text of the hexagon
     * @param h3Index the H3 index to set the text of the hexagon
     */
    private setHexagonText(h3Cell: string) {
        const h3IndexInfo = new H3IndexInfo(h3Cell);

        (this.getStyle() as Style).setText(
            new Text({
                text: convertNumberIdToLettersId(Number(h3IndexInfo.getAugmentedLiteral().split('-')[0])) + '-' + h3IndexInfo.getAugmentedLiteral().split('-').slice(1).join('-'),
            })
        );
    }

    // ========================================= GETTERS & SETTERS ======================================

    /**
     * Set the tag of the hexagon
     * @param tag the tag to set to the hexagon
     */
    public setHexagonTag(tag: string) {
        if (tag !== undefined) {
            this.hexagonTag = tag;
        }
    }

    /**
     * Get the tag of the hexagon
     * @returns the tag of the hexagon
     */
    public getHexagonTag() {
        return this.hexagonTag;
    }

    /**
     * Get the H3 cell of the hexagon
     * @returns the H3 cell of the hexagon
     */
    public getH3Cell(): string {
        return this.h3Cell;
    }

    // ========================================= STATIC METHODS =========================================

    /**
     * Create a list of HexagonFeature from a list of H3 cells.
     * @param h3List the list of H3 cells
     * @param setText boolean to set or not the text of the hexagons
     * @returns a list of HexagonFeature
     */
    public static createHexagonFeatures(h3CellList: string[], setText = false): HexagonFeature[] {
        const features = h3CellList.map(h3Cell => {
            const feature = new HexagonFeature(h3Cell);
            if (setText) {
                feature.setHexagonText(h3Cell);
            }
            return feature;
        });

        return features;
    }

    /**
     * Get the points in a list of hexagons
     * @param hexagons the list of hexagons to get the points in
     * @returns a list of points in the hexagons
     */
    public static getPointsInListOfH3Cells(h3CellList: string[], pointsList: PointFeature[]) {
        const points: PointFeature[] = [];

        h3CellList.forEach(h3Index => {
            const tmp = HexagonFeature.getPointsInPolygonFeature(h3Index, pointsList);
            tmp.forEach(point => {
                if (!points.includes(point)) {
                    points.push(point);
                }
            });
        });

        return points;
    }
}