import { HexagonFeature } from "./HexagonFeature";

/**
 * HexagonFeatureWithValue is a HexagonFeature that has a value.
 */
export class HexagonFeatureWithValue extends HexagonFeature {
    constructor(h3cell: string, value: number) {
        super(h3cell)
        this.set('value', value);
    }

    /**
     * Create a list of HexagonFeatureWithValue from a list of h3 indexes and their values
     * @param h3ListWithValues the h3 indexes and their values
     * @returns a list of HexagonFeatureWithValue
     */
    public static createHexagonFeaturesWithValue(h3ListWithValues: { [h3Index: string]: number }): HexagonFeatureWithValue[] {
        const h3CellFeatures = Object.keys(h3ListWithValues).map((h3Index) => {
            const feature = new HexagonFeatureWithValue(h3Index, h3ListWithValues[h3Index]);
            return feature;
        });
        return h3CellFeatures;
    }
}