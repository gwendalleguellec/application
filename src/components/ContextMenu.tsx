import { DataExporter } from '../services/DataExporter';
import { renderToStaticMarkup } from "react-dom/server"

/**
 * ContextMenu component
 */
export default function ContextMenu(contextmenu: HTMLElement, event: any, h3Cell: string, h3CellAndItTag: { [h3Cell: string]: string }, setH3CellAndItsTag: any, hexagonsClicked: string[]) {

  /**
   * Set the style and the position of the context menu
   */
  const setStyleContextMenu = () => {
    contextmenu.style.display = 'flex';

    contextmenu.style.top = `${event.originalEvent.clientY}px`;
    contextmenu.style.left = `${event.originalEvent.clientX}px`;

    // Manage the position of the contextmenu when it is out of the window
    const contextmenuRect = contextmenu.getBoundingClientRect();
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;
    const contextmenuWidth = contextmenuRect.width;
    const contextmenuHeight = contextmenuRect.height;
    const contextmenuX = event.originalEvent.clientX;
    const contextmenuY = event.originalEvent.clientY;
    const contextmenuXEnd = contextmenuX + contextmenuWidth;
    const contextmenuYEnd = contextmenuY + contextmenuHeight;
    const contextmenuXOffset = contextmenuXEnd - windowWidth;
    const contextmenuYOffset = contextmenuYEnd - windowHeight;
    if (contextmenuXOffset > 0) {
      contextmenu.style.left = `${contextmenuX - contextmenuXOffset}px`;
    }
    if (contextmenuYOffset > 0) {
      contextmenu.style.top = `${contextmenuY - contextmenuYOffset}px`;
    }
  };

  /**
   * Handle the context menu when the user right-click on a hexagon cell selected or outside the set of hexagons selected
   */
  const handleTwoWaysToSetContextMenu = () => {
    if (!hexagonsClicked.includes(h3Cell)) {
      exportMenu(); // export the list of clicked hexagons in a CSV file
    } else {
      validateMenu(); // validate the tag of the hexagon cell
    }
  };

  /**
   * Create the export menu
   */
  const exportMenu = () => {
    contextmenu.innerHTML = contextMenuContentExport();

    const exportTag = document.getElementById('exportTag') as HTMLButtonElement;
    exportTag.focus();

    contextmenu.addEventListener('keydown', (e) => {
      if (e.key === 'Escape') { // hide the contextmenu
        contextmenu.style.display = 'none';
      }
    });

    // export the list clicked hexagons in a CSV file with their tag if they have one
    exportTag.addEventListener('click', () => {
      const header = 'h3,tag';
      const data = hexagonsClicked.map(h3Cell => `${h3Cell},${h3CellAndItTag[h3Cell] ? h3CellAndItTag[h3Cell] : ''}`);
      const fileName = 'zone_with_h3_tagged.csv';
      DataExporter.createCSVFile(header, data, fileName);
    });

    const closeContextmenu = document.getElementById('closeContextmenu') as HTMLButtonElement;
    closeContextmenu.addEventListener('click', () => {
      contextmenu.style.display = 'none';
    });
  };

  /**
   * Create the validate menu
   */
  const validateMenu = () => {
    contextmenu.innerHTML = contextMenuContentValidate();

    const input = document.getElementById('hexagonTag') as HTMLInputElement;
    input.focus();

    contextmenu.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') { // validate the tag and hide the contextmenu
        validateTag(input);
      }
      if (e.key === 'Escape') { // hide the contextmenu
        contextmenu.style.display = 'none';
      }
    });

    const validateTagButton = document.getElementById('validateTag') as HTMLButtonElement;
    validateTagButton.addEventListener('click', () => { // validate the tag and hide the contextmenu
      validateTag(input);
    });

    const closeContextmenu = document.getElementById('closeContextmenu') as HTMLButtonElement;
    closeContextmenu.addEventListener('click', () => {
      contextmenu.style.display = 'none';
    });
  };

  /**
   * Validate the tag of the hexagon cell
   * @param input input HTML element
   */
  const validateTag = (input: HTMLInputElement) => {
    const tag = input.value;
    contextmenu.style.display = 'none';
    const newH3CellAndItsTag = { ...h3CellAndItTag };
    newH3CellAndItsTag[h3Cell] = tag;
    setH3CellAndItsTag(newH3CellAndItsTag);
  };

  /**
   * Render the content of the export menu
   */
  const contextMenuContentExport = () => {
    return renderToStaticMarkup(
      <>
        <div>
          <p className="title">Exporter la zone en CSV (h3, tag)</p>
          <button id="closeContextmenu">&times;</button>
        </div>
        <button id="exportTag">Exporter</button>
      </>
    )
  };

  /**
   * Render the content of the validate menu
   */
  const contextMenuContentValidate = () => {
    return renderToStaticMarkup(
      <>
        {
          h3CellAndItTag[h3Cell] ?
            <>
              <div>
                <p className="title">Modifier le tag de la cellule</p>
                <button id="closeContextmenu">&times;</button>
              </div>
              <input type="text" id="hexagonTag" placeholder="Nom du tag" value={h3CellAndItTag[h3Cell]} />
              <button id="validateTag">Modifier</button>
            </> :

            <>
              <div>
                <p className="title">Ajouter un tag à la cellule</p>
                <button id="closeContextmenu">&times;</button>
              </div>
              <input type="text" id="hexagonTag" placeholder="Nom du tag" />
              <button id="validateTag">Valider</button>
            </>
        }
      </>
    )
  };

  setStyleContextMenu();
  handleTwoWaysToSetContextMenu();
}