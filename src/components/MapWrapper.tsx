import { useEffect, useState, useRef, useCallback } from 'react';

import 'ol/ol.css';
import '../styles/MapWrapper.css';
import Map from 'ol/Map';
import View from 'ol/View';
import OSM from 'ol/source/OSM';
import { toLonLat, fromLonLat } from 'ol/proj';
import { toStringXY } from 'ol/coordinate';

import TileLayer from 'ol/layer/Tile';
import { Feature } from 'ol';
import { Group as LayerGroup } from 'ol/layer';
import { Geometry } from 'ol/geom';

import { SimpleH3GridLayer } from '../models/grids/SimpleH3GridLayer';
import { SquareGridLayer } from '../models/grids/SquareGridLayer';
import { H3GridLayer } from '../models/grids/H3GridLayer'
import { GridLayer } from '../models/grids/GridLayer';
import { HexagonFeature } from '../models/features/HexagonFeature';
import { SquareFeature } from '../models/features/SquareFeature';
import { PointFeature } from '../models/features/PointFeature';

import { cellToParent, latLngToCell } from 'h3-js';
import { H3IndexInfo } from '../models/h3/H3IndexInfo';
import { getH3Resolution } from '../models/h3/H3Utilities';

import ContextMenu from './ContextMenu';

import { MapWrapperProps } from '../interfaces/MapWrapperProps';

const center = fromLonLat([-2.8362402, 47.8874274]); // Coordinates of the center of the map : Locminé
const zoom = 8.8; // Zoom by default

export default function MapWrapper(props: Readonly<MapWrapperProps>) { // props send from App.tsx
  const [map, setMap] = useState<Map>();
  const [selectedCoord, setSelectedCoord] = useState<string>("");
  const [h3String, setH3String] = useState<string>("");
  const [hexagonsClicked, setHexagonsClicked] = useState<string[]>([]);
  const [squaresClicked, setSquaresClicked] = useState<SquareFeature[]>([]);
  const [h3CellAndItsTag, setH3CellAndItsTag] = useState<{ [h3Cell: string]: string }>({});
  const [h3CurrentCell, setH3CurrentCell] = useState<string>("");

  const mapElement = useRef<HTMLDivElement | null>(null);
  const mapRef = useRef<Map>();
  mapRef.current = map;

  useEffect(() => { // initialize the map
    const osm = new TileLayer({
      source: new OSM(),
    });

    const layerGroup = new LayerGroup({
      layers: [],
    });

    const initialMap = new Map({
      target: mapElement.current as HTMLDivElement,
      layers: [osm, layerGroup],
      view: new View({
        center: center,
        zoom: zoom,
      }),
    });

    const h3GridLayer = new H3GridLayer({}, initialMap);
    h3GridLayer.refreshGrid();
    initialMap.addLayer(h3GridLayer);
    h3GridLayer.setVisible(false); // hide grid by default

    const simpleGridLayer = new SimpleH3GridLayer({}, initialMap);
    simpleGridLayer.refreshGrid();
    initialMap.addLayer(simpleGridLayer);
    simpleGridLayer.setVisible(false); // hide grid by default

    const squareGridLayer = new SquareGridLayer({}, initialMap);
    squareGridLayer.refreshGrid();
    initialMap.addLayer(squareGridLayer);
    squareGridLayer.setVisible(false);

    initialMap.on('pointermove', handlePointerMove);
    setMap(initialMap);
    setH3CurrentCell(latLngToCell(center[1], center[0], getH3Resolution(initialMap) + 1));
  }, []);

  // ======================================== Handle click and move events ========================================

  /**
   * Handle the pointer move event on the map to display the coordinates and the h3 index
   * @param event the pointer move event
   */
  const handlePointerMove = (event: any) => {
    if (mapRef.current) {
      const clickedCoord = mapRef.current.getCoordinateFromPixel(event.pixel);
      const transformedCoord = toLonLat(clickedCoord).reverse();
      const h3Cell = latLngToCell(transformedCoord[0], transformedCoord[1], getH3Resolution(mapRef.current) + 1);
      const h3IndexInfo = new H3IndexInfo(h3Cell);
      const h3String = h3IndexInfo.getAugmentedLiteral(); // .getLiteral().join('-')
      setSelectedCoord(toStringXY(transformedCoord, 5));
      setH3String(h3String);

      const h3CellParent = cellToParent(h3Cell, getH3Resolution(mapRef.current));
      setH3CurrentCell(h3CellParent);
    }
  }

  /**
   * Handle the display of the context menu when the user right click on a hexagon
   * @param event the click event
   * @param h3Cell the h3 cell
   */
  const handleDisplayOfContextMenu = useCallback((event: any, h3Cell: string) => {
    const contextmenu = document.getElementById('contextmenu') as HTMLDivElement;
    ContextMenu(contextmenu, event, h3Cell, h3CellAndItsTag, setH3CellAndItsTag, hexagonsClicked);
  }, [h3CellAndItsTag, hexagonsClicked]);

  /**
   * Catch the points in the clicked hexagons or squares
   */
  const catchPoints = useCallback((listFeatures: any[]) => {
    let listPoints: Feature<Geometry>[] = [];

    if (listFeatures[0] instanceof SquareFeature) {
      listPoints = SquareFeature.getPointsInListOfSquares(listFeatures as SquareFeature[], props.layers['points'] as PointFeature[]);
    } else if (typeof listFeatures[0] === 'string') {
      listPoints = HexagonFeature.getPointsInListOfH3Cells(listFeatures as string[], props.layers['points'] as PointFeature[]);
    }

    PointFeature.listPointsInTheMenu(listPoints, 'listPoints');

    props.layers['points'].forEach((point: Feature<Geometry>) => {
      if (!listPoints.includes(point)) {
        point.setStyle(); // reset the style of the point
      }
    });

    props.setPointsSelected(listPoints as PointFeature[]); // to manage selected points in the menu

    if (listFeatures[0] instanceof SquareFeature) {
      const squareGridLayer = mapRef.current?.getLayers().item(4) as SquareGridLayer;
      squareGridLayer.refreshGrid();
    } else if (typeof listFeatures[0] === 'string') {
      const simpleGridLayer = mapRef.current?.getLayers().item(3) as H3GridLayer;
      simpleGridLayer.refreshGrid();
    }
  }, [props]);

  /**
   * Highlight the clicked features on the map (hexagons or squares)
   * @param listFeatures the list of features to highlight
   */
  const highlightClickedFeatures = useCallback((listFeatures: Feature<Geometry>[]) => {
    const layerGroup = mapRef.current?.getLayers().item(1) as LayerGroup;
    const layers = layerGroup.getLayers().getArray();
    layers.pop();

    if (listFeatures[0] instanceof HexagonFeature) {
      (listFeatures).forEach(hexagon => {
        const hexagonFeature = hexagon as HexagonFeature;
        // Set the tag of the hexagon if it was clicked
        if (h3CellAndItsTag[hexagonFeature.getH3Cell()]) {
          hexagonFeature.setHexagonTag(h3CellAndItsTag[hexagonFeature.getH3Cell()]);
        }
        hexagonFeature.setCaughtPolygonStyle();
      });
    } else if (listFeatures[0] instanceof SquareFeature) {
      listFeatures.forEach(square => {
        const squareFeature = square as SquareFeature;
        squareFeature.setCaughtPolygonStyle();
      });
    }

    GridLayer.addLayerToLayerGroup(layerGroup, listFeatures);
  }, [h3CellAndItsTag]);

  /**
   * Handle the click event on the map to select a cell (hexagon)
   * @param event the click event
   */
  const handlePointerClick = useCallback((event: any) => {
    if (mapRef.current) {
      event.preventDefault(); // Prevent the default right click event


      if (event.type === 'click') {
        let newList: string[] = [...hexagonsClicked];
        if (!newList.includes(h3CurrentCell)) {
          newList.push(h3CurrentCell);
        } else {
          newList.splice(newList.indexOf(h3CurrentCell), 1);
        }

        const listOfHexagons: HexagonFeature[] = HexagonFeature.createHexagonFeatures(newList); // Create the list of hexagons with the h3 index
        setHexagonsClicked(newList);
        highlightClickedFeatures(listOfHexagons);
        catchPoints(newList);
      } else if (event.type === 'contextmenu') { // right click on the hexagon
        event.preventDefault(); // Prevent the default right click event
        handleDisplayOfContextMenu(event, h3CurrentCell);
      } else {
        const contextmenu = document.getElementById('contextmenu') as HTMLDivElement;
        contextmenu.style.display = 'none';
        contextmenu.classList.remove('show-contextmenu');
      }
    }
  }, [highlightClickedFeatures, catchPoints, handleDisplayOfContextMenu, hexagonsClicked, h3CurrentCell]);

  /**
   * Handle the click event on the map to select a square and catch the points in the square
   * @param event the click event
   */
  const handleSquareClick = useCallback((event: any) => {
    if (mapRef.current) {
      const squareGridLayer = mapRef.current.getLayers().item(4) as SquareGridLayer;
      const squareFeature = squareGridLayer.getSource()?.getFeaturesAtCoordinate(event.coordinate)[0] as SquareFeature;

      let newList: SquareFeature[] = [...squaresClicked];

      if (!squaresClicked.some(square => square.hasSameCoordinates(squareFeature))) {
        newList.push(squareFeature);
      } else {
        newList = squaresClicked.filter(square => !square.hasSameCoordinates(squareFeature));
      }

      setSquaresClicked(newList);
      highlightClickedFeatures(newList);
      catchPoints(newList);
    }
  }, [squaresClicked, highlightClickedFeatures, catchPoints]);

  // =============================================== UseEffect ================================================

  useEffect(() => { // click event on the button to cancel the selection
    const buttonAnnuler = document.getElementById('cancelSelect') as HTMLButtonElement;
    buttonAnnuler.addEventListener('click', () => {
      setHexagonsClicked([]);
      setSquaresClicked([]);
      props.setPointsSelected([]);
      props.setFilteredList1([]);
      props.setFilteredList2([]);
      props.setResultList([]);
      props.setFirstListValidated(false);
    });
  }, [props.pointsSelected, props]);

  useEffect(() => { // click event on the button to validate the selection
    const validateSelect = document.getElementById('validateSelect') as HTMLButtonElement;
    validateSelect.addEventListener('click', () => {
      setHexagonsClicked([]);
      setSquaresClicked([]);
    });
  }, [props.pointsSelected, props]);

  useEffect(() => { // click event on the map to select a square
    if (props.squareGrid && !props.simpleGrid && !props.h3Grid) {
      mapRef.current?.on('click', handleSquareClick);
    }
  }, [handleSquareClick, squaresClicked, props]);

  useEffect(() => { // click event on the map to select a hexagon / set the tag of the hexagon
    if (props.simpleGrid && !props.squareGrid && !props.h3Grid) {
      const handleClick = (event: any) => handlePointerClick(event);
      const handleContextMenu = (event: any) => handlePointerClick(event);

      if (mapRef.current) {
        mapRef.current.on('click', handleClick);
        mapRef.current.addEventListener('contextmenu', handleContextMenu);
      }
      return () => {
        mapRef.current?.un('click', handleClick);
        mapRef.current?.removeEventListener('contextmenu', handleContextMenu);
      };
    }
  }, [handlePointerClick, props.h3Grid, props.simpleGrid, props.squareGrid]);

  useEffect(() => { // update the tag
    if (mapRef.current) {
      const layerGroup = mapRef.current.getLayers().item(1) as LayerGroup;
      const layers = layerGroup.getLayers().getArray();
      layers.forEach((layer) => {
        const features = (layer as any).getSource().getFeatures();
        const hexagons = features.filter((feature: any) => feature instanceof HexagonFeature);
        hexagons.forEach((hexagon: any) => {
          const h3Cell = hexagon.getH3Cell();
          hexagon.setHexagonTag(h3CellAndItsTag[h3Cell]);
          hexagon.setCaughtPolygonStyle();
        });
      });
    }
  }, [hexagonsClicked, props.layers, h3CellAndItsTag]);

  useEffect(() => { // update the LayerGroup when the props.layers change
    if (mapRef.current) {
      GridLayer.setLayersToLayerGroup(mapRef.current.getLayers().item(1) as LayerGroup, props.layers);
    }
  }, [props.layers]);

  useEffect(() => { // show/hide the h3 grid
    mapRef.current?.getLayers().item(2)?.setVisible(props.h3Grid);
  }, [props.h3Grid]);

  useEffect(() => { // show/hide the simple grid
    mapRef.current?.getLayers().item(3)?.setVisible(props.simpleGrid);
  }, [props.simpleGrid]);

  useEffect(() => { // show/hide the square grid
    mapRef.current?.getLayers().item(4)?.setVisible(props.squareGrid);
  }, [props.squareGrid]);

  useEffect(() => { // update the zoom level
    mapRef.current?.getView().setZoom(props.zoomMap);
  }, [props.zoomMap]);

  useEffect(() => { // update the center of the map
    mapRef.current?.getView().setCenter(fromLonLat(props.mapCenter));
  }, [props.mapCenter]);

  return (
    <div>
      <div ref={mapElement} className="map-container" />
      <div id="contextmenu" className='show-contextmenu'></div>
      <div className="clicked-coord-label sub-menu">
        <p><b>Coords:</b> {selectedCoord}</p>
        <p><b>H3: </b>{h3String}</p>
        <p><b>Couleurs grilles H3:</b></p>
        <ul>
          <li><p id="colorRes1">Résolution impaire</p></li>
          <li><p id="colorRes2">Résolution paire</p></li>
          <li><p id="colorRes3">Résolution 5</p></li>
        </ul>
      </div>
    </div >
  );
}