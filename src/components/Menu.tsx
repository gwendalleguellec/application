import '../styles/Menu.css';
import { Feature } from 'ol';
import { Geometry } from 'ol/geom';
import { PointFeature } from '../models/features/PointFeature';
import { DataFetcher } from '../services/DataFetcher';
import { DataExporter } from '../services/DataExporter';
import { CSVPointsData } from '../types/DataFetcherTypes';
import { MenuProps } from '../interfaces/MenuProps';

export default function Menu(props: Readonly<MenuProps>) {

  const hide = () => {
    reset();
    props.setH3Grid(false);
    props.setSimpleGrid(false);
    props.setSquareGrid(false);
    const hide = document.getElementById('hide') as HTMLInputElement;
    hide.checked = true;
  };

  const showH3Grid = () => {
    reset();
    props.setSquareGrid(false);
    props.setSimpleGrid(false);
    props.setH3Grid(true);
    const grid = document.getElementById('grid') as HTMLInputElement;
    grid.checked = true;

    const hide = document.getElementById('hide') as HTMLInputElement;
    hide.checked = false;
  };

  const showSimpleGrid = () => {
    reset();
    props.setSquareGrid(false);
    props.setH3Grid(false);
    props.setSimpleGrid(true);
    const simpleGrid = document.getElementById('simpleGrid') as HTMLInputElement;
    simpleGrid.checked = true;

    const hide = document.getElementById('hide') as HTMLInputElement;
    hide.checked = false;
  };

  const showSquareGrid = () => {
    reset();
    props.setSimpleGrid(false);
    props.setH3Grid(false);
    props.setSquareGrid(true);
    const squareGrid = document.getElementById('squareGrid') as HTMLInputElement;
    squareGrid.checked = true;

    const hide = document.getElementById('hide') as HTMLInputElement;
    hide.checked = false;
  };

  const showOrHidePoints = (): void => {
    if (props.layers['points'].length > 0) {
      props.setLayers({ ...props.layers, 'points': [] });
    } else {
      DataFetcher.getPointsCSVData('points.csv').then((obj: CSVPointsData) => {
        const features = PointFeature.createPointFeatures(obj.data);
        props.setLayers({ ...props.layers, 'points': features });
      });
    }
  };

  const reset = (): void => {
    const points = document.getElementById('listPoints') as HTMLElement;
    points.innerHTML = '';
    const points2 = document.getElementById('listPoints2') as HTMLElement;
    points2.innerHTML = '';
    const result = document.getElementById('listResult') as HTMLElement;
    result.innerHTML = '';

    props.setH3Grid(false);
    props.setSimpleGrid(false);
    props.setSquareGrid(false);

    const gridType = document.getElementsByName('gridType') as NodeListOf<HTMLInputElement>;
    gridType.forEach(radio => {
      radio.checked = false;
    });

    const pointsCheckbox = document.getElementById('points') as HTMLInputElement;
    if (pointsCheckbox.checked) {
      DataFetcher.getPointsCSVData('points.csv').then((obj: CSVPointsData) => {
        const features = PointFeature.createPointFeatures(obj.data);
        props.setLayers({ ...props.layers, 'points': features });
      });
    }

    props.setFilteredList1([]);
    props.setFilteredList2([]);
    props.setResultList([]);
  };

  const filterPointsByFlag = (points: Feature<Geometry>[], country: string): Feature<Geometry>[] => {
    const flag = PointFeature.country2flag(country);
    const pointsFilteredByFlag = points.filter(point => PointFeature.country2flag(point.get('data').flag) === flag);

    return pointsFilteredByFlag;
  };

  const filterList = (): void => {
    const country = prompt("Enter country for list 1:");
    if (country) {
      try {
        const filteredPoints = filterPointsByFlag(props.pointsSelected, country);
        if (!props.firstListValidated) {
          props.setFilteredList1(filteredPoints);
        } else {
          props.setFilteredList2(filteredPoints);
        }
        PointFeature.listPointsInTheMenu(filteredPoints, 'listPoints');
      } catch (error) {
        console.error('Error filtering points:', error);
      }
    }
  };

  const validateList = (): void => {
    props.setFirstListValidated(true);

    const listPoints = document.getElementById('listPoints') as HTMLElement;
    let points;

    if (!props.firstListValidated) {
      points = document.getElementById('listPoints1') as HTMLElement;
      points.innerHTML = listPoints.innerHTML;
      listPoints.innerHTML = '';
      props.setFilteredList1(props.pointsSelected);
      showList1();
    } else {
      points = document.getElementById('listPoints2') as HTMLElement;
      points.innerHTML = listPoints.innerHTML;
      listPoints.innerHTML = '';
      props.setFilteredList2(props.pointsSelected);
      showList2();
      const points1 = document.getElementById('listPoints1') as HTMLElement;
      points1.style.display = 'none';
    }

    // grise la première liste
    const li = points.getElementsByTagName('li') as unknown as HTMLLIElement[];
    for (const element of li) {
      element.style.color = 'gray';
      element.style.backgroundColor = 'lightgray';
      element.style.borderColor = 'gray';
    }

    // reset la map
    DataFetcher.getPointsCSVData('points.csv').then((obj: CSVPointsData) => {
      const features = PointFeature.createPointFeatures(obj.data);
      const pointsCheckbox = document.getElementById('points') as HTMLInputElement;
      if (pointsCheckbox.checked) {
        props.setLayers({ ...props.layers, 'points': features });
      } else {
        props.setLayers({ ...props.layers, 'points': [] });
      }
    });
  };

  const resetList = (): void => {
    props.setFilteredList1([]);
    props.setFilteredList2([]);
    props.setResultList([]);

    const points1 = document.getElementById('listPoints1') as HTMLElement;
    points1.innerHTML = '';
    const points2 = document.getElementById('listPoints2') as HTMLElement;
    points2.innerHTML = '';
    const result = document.getElementById('listResult') as HTMLElement;
    result.innerHTML = '';

    // à adapter lorsque les points ne sont plus affichés
    DataFetcher.getPointsCSVData('points.csv').then((obj: CSVPointsData) => {
      const features = PointFeature.createPointFeatures(obj.data);
      const pointsCheckbox = document.getElementById('points') as HTMLInputElement;
      if (pointsCheckbox.checked) {
        props.setLayers({ ...props.layers, 'points': features });
      } else {
        props.setLayers({ ...props.layers, 'points': [] });
      }
    });

    const listPoints = document.getElementById('listPoints') as HTMLElement;
    listPoints.innerHTML = '';

    const points = document.getElementById('listPoints1') as HTMLElement;
    points.innerHTML = '';
  };

  const unionLists = (): void => {
    let union: Feature<Geometry>[] = [...props.filteredList1, ...props.filteredList2];

    union = union.filter((point, index, self) =>
      index === self.findIndex((t) => (
        t.get('data').flag === point.get('data').flag &&
        t.get('data').timestamp === point.get('data').timestamp &&
        t.get('data').mmsi === point.get('data').mmsi &&
        t.get('data').name === point.get('data').name &&
        t.get('data').type === point.get('data').type
      ))
    );

    props.setResultList(union);
    PointFeature.listPointsInTheMenu(union, 'listResult');

    const unionOrIntersect = document.getElementById('unionOrIntersect') as HTMLElement;
    unionOrIntersect.innerHTML = 'Union';
  };

  const intersectLists = (): void => { // à revoir !!!!!!!!!
    let intersection: Feature<Geometry>[] = [];

    props.filteredList1.forEach(point1 => {
      props.filteredList2.forEach(point2 => {
        if (point1.get('data').flag === point2.get('data').flag &&
          point1.get('data').timestamp === point2.get('data').timestamp &&
          point1.get('data').mmsi === point2.get('data').mmsi &&
          point1.get('data').name === point2.get('data').name &&
          point1.get('data').type === point2.get('data').type) {
          intersection.push(point1);
        }
      });
    });

    props.setResultList(intersection);
    PointFeature.listPointsInTheMenu(intersection, 'listResult');

    const unionOrIntersect = document.getElementById('unionOrIntersect') as HTMLElement;
    unionOrIntersect.innerHTML = 'Intersection';
  };

  const exportCSV = (): void => {
    const header = 'Flag,Timestamp,MMSI,Name,Type';
    
    const data: string[] = [];
    props.resultList.forEach(point => {
      const pointData = point.get('data');
      data.push(`${pointData.flag},${pointData.timestamp},${pointData.mmsi},${pointData.name},${pointData.type}`);
    });
    
    let filename = '';
    const unionOrIntersect = document.getElementById('unionOrIntersect') as HTMLElement;
    if (unionOrIntersect.innerHTML === 'Union') {
      filename = 'union.csv';
    } else {
      filename = 'intersect.csv';
    }

    DataExporter.createCSVFile(header, data, filename);
  };

  const showList1 = (): void => {
    const tabList1 = document.getElementsByClassName('tab')[0] as HTMLElement;
    tabList1.style.borderBottom = '2px solid steelblue';
    tabList1.style.paddingTop = '7px';

    const tabList2 = document.getElementsByClassName('tab')[1] as HTMLElement;
    tabList2.style.borderBottom = 'none';
    tabList2.style.paddingTop = '5px';

    const tabResult = document.getElementsByClassName('tab')[2] as HTMLElement;
    tabResult.style.borderBottom = 'none';
    tabResult.style.paddingTop = '5px';


    const points = document.getElementById('listPoints1') as HTMLElement;
    points.style.display = 'block';

    // hide list 2 and result
    const points2 = document.getElementById('listPoints2') as HTMLElement;
    points2.style.display = 'none';
    const result = document.getElementById('result') as HTMLElement;
    result.style.display = 'none';
  };

  const showList2 = (): void => {
    const tabList1 = document.getElementsByClassName('tab')[0] as HTMLElement;
    tabList1.style.borderBottom = 'none';
    tabList1.style.paddingTop = '5px';

    const tabList2 = document.getElementsByClassName('tab')[1] as HTMLElement;
    tabList2.style.borderBottom = '2px solid steelblue';
    tabList2.style.paddingTop = '7px';

    const tabResult = document.getElementsByClassName('tab')[2] as HTMLElement;
    tabResult.style.borderBottom = 'none';
    tabResult.style.paddingTop = '5px';

    const points = document.getElementById('listPoints2') as HTMLElement;
    points.style.display = 'block';

    // hide list 1 and result
    const points1 = document.getElementById('listPoints1') as HTMLElement;
    points1.style.display = 'none';
    const result = document.getElementById('result') as HTMLElement;
    result.style.display = 'none';
  };

  const showListResult = (): void => {
    const tabList1 = document.getElementsByClassName('tab')[0] as HTMLElement;
    tabList1.style.borderBottom = 'none';
    tabList1.style.paddingTop = '5px';

    const tabList2 = document.getElementsByClassName('tab')[1] as HTMLElement;
    tabList2.style.borderBottom = 'none';
    tabList2.style.paddingTop = '5px';

    const tabResult = document.getElementsByClassName('tab')[2] as HTMLElement;
    tabResult.style.borderBottom = '2px solid steelblue';
    tabResult.style.paddingTop = '7px';

    const points = document.getElementById('result') as HTMLElement;
    points.style.display = 'block';

    // hide list 1 and 2
    const points1 = document.getElementById('listPoints1') as HTMLElement;
    points1.style.display = 'none';
    const points2 = document.getElementById('listPoints2') as HTMLElement;
    points2.style.display = 'none';
  };

  return (
    <div className="Menu">
      <div className="sub-menu">
        <h1>Sélection d'éléments sur une carte</h1>
        <p className='title'>Comparaison entre carrés et hexagones</p>
        <div id="grids">
          <h2>Grilles</h2>
          <div>
            <input name="gridType" type="radio" id="grid" onClick={showH3Grid} />
            <label htmlFor="grid" id="gridLabel">Afficher la grille hexagonale avec plusieurs résolutions</label>
          </div>
          <div>
            <input name="gridType" type="radio" id="simpleGrid" onClick={showSimpleGrid} />
            <label htmlFor="simpleGrid" id="simpleGridLabel">Afficher la grille hexagonale simple</label>
          </div>
          <div>
            <input name="gridType" type="radio" id="squareGrid" onClick={showSquareGrid} />
            <label htmlFor="squareGrid" id="squareGridLabel">Afficher la grille carrée</label>
          </div>
          <div>
            <input type="radio" id="hide" onClick={hide} defaultChecked />
            <label htmlFor="hide" id="hideLabel">Ne pas afficher les grilles</label>
          </div>
          <div>
            <input type="checkbox" id="points" onClick={showOrHidePoints} />
            <label htmlFor="points" id="pointsLabel">Afficher les points</label>
          </div>
        </div>

        <div id="selection">
          <h2>Objets sélectionnés </h2>
          <p className='title'>(Flag, Timestamp, MMSI, Name, Type)</p>

          <div id='buttonsFilterValidateReset'>
            <button className='button' onClick={filterList} id="filterSelect">Filtrer par un pays</button>
            <button className='button' onClick={validateList} id='validateSelect'>Valider</button>
            <button className='button' onClick={resetList} id='cancelSelect'>Annuler</button>
          </div>

          <ul id="listPoints"></ul>
        </div>

        <div id='listsWithTabs'>
          <h2>Union / Intersection</h2>
          <div id='tabs'>
            <button className='title button tab' onClick={showList1}>Liste 1</button>
            <button className='title button tab' onClick={showList2}>Liste 2</button>
            <button className='title button tab' onClick={showListResult}>Résultat</button>
          </div>

          <ul id="listPoints1"></ul>
          <ul id="listPoints2"></ul>
          <div id='result'>
            <br />
            <div id="buttonsUnionIntersectExport">
              <button className='button' onClick={unionLists} id="unionButton">Union des listes</button>
              <button className='button' onClick={intersectLists} id="intersectButton">Intersection des listes</button>
              <button className='button' onClick={exportCSV} id="exportButton">Exporter CSV</button>
            </div>
            <p className='title' id="unionOrIntersect"></p>
            <div id='lists'>
              <ul id="listResult"></ul>
            </div>
          </div>
          <br />
        </div>
      </div>
    </div>
  );
};
