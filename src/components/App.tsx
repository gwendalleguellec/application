import { useState } from 'react';
import '../styles/App.css';

import MapWrapper from './MapWrapper';
import Menu from './Menu';
import Feature from 'ol/Feature';
import { Geometry } from 'ol/geom';
import { PointFeature } from '../models/features/PointFeature';

export default function App() {
  const [squareGrid, setSquareGrid] = useState<boolean>(false); // used to show/hide the square grid
  const [simpleGrid, setSimpleGrid] = useState<boolean>(false); // used to show/hide the simple grid
  const [h3Grid, setH3Grid] = useState<boolean>(false); // used to show/hide the h3 grid
  const [layers, setLayers] = useState<{ [model: string]: Feature<Geometry>[] }>({ // dictionary of layers to display on the map
    'points': [], // layer to display points
    'other': [] // don't remove this layer ?!
  });
  const [pointsSelected, setPointsSelected] = useState<PointFeature[]>([]); // Points in the clicked hexagons or squares
  const [zoomMap] = useState<number>(8.8);
  const [mapCenter] = useState<number[]>([-2.8362402, 47.8874274]);

  const [filteredList1, setFilteredList1] = useState<Feature<Geometry>[]>([]);
  const [filteredList2, setFilteredList2] = useState<Feature<Geometry>[]>([]);
  const [resultList, setResultList] = useState<Feature<Geometry>[]>([]);
  const [firstListValidated, setFirstListValidated] = useState<boolean>(false);

  return (
    <div className="App">
      <Menu 
        setH3Grid={setH3Grid} 
        setSimpleGrid={setSimpleGrid} 
        setSquareGrid={setSquareGrid} 
        layers={layers}
        setLayers={setLayers}
        pointsSelected={pointsSelected}
        filteredList1={filteredList1}
        setFilteredList1={setFilteredList1}
        filteredList2={filteredList2}
        setFilteredList2={setFilteredList2}
        resultList={resultList}
        setResultList={setResultList}
        firstListValidated={firstListValidated}
        setFirstListValidated={setFirstListValidated}
      />
      <MapWrapper 
        layers={layers} 
        h3Grid={h3Grid} 
        zoomMap={zoomMap} 
        mapCenter={mapCenter} 
        simpleGrid={simpleGrid} 
        squareGrid={squareGrid}
        pointsSelected={pointsSelected}
        setPointsSelected={setPointsSelected}
        setFilteredList1={setFilteredList1}
        setFilteredList2={setFilteredList2}
        setResultList={setResultList}
        setFirstListValidated={setFirstListValidated}
      />
    </div>
  );
}