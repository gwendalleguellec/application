import { Feature } from 'ol';
import { Geometry } from 'ol/geom';
import { PointFeature } from '../models/features/PointFeature';

export interface MenuProps {
    readonly setH3Grid: (h3Grid: boolean) => void;
    readonly setSimpleGrid: (simpleGrid: boolean) => void;
    readonly setSquareGrid: (squareGrid: boolean) => void;
    readonly layers: { [model: string]: Feature<Geometry>[] };
    readonly setLayers: (layers: { [model: string]: Feature<Geometry>[] }) => void;
    readonly pointsSelected: PointFeature[];
    readonly filteredList1: Feature<Geometry>[];
    readonly setFilteredList1: (filteredList1: Feature<Geometry>[]) => void;
    readonly filteredList2: Feature<Geometry>[];
    readonly setFilteredList2: (filteredList2: Feature<Geometry>[]) => void;
    readonly resultList: Feature<Geometry>[];
    readonly setResultList: (resultList: Feature<Geometry>[]) => void;
    readonly firstListValidated: boolean;
    readonly setFirstListValidated: (firstListValidated: boolean) => void;
}