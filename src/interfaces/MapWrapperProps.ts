import { Feature } from 'ol';
import { Geometry } from 'ol/geom';
import { PointFeature } from '../models/features/PointFeature';

export interface MapWrapperProps {
    readonly layers: { [model: string]: Feature<Geometry>[] };
    readonly h3Grid: boolean;
    readonly zoomMap: number;
    readonly mapCenter: number[];
    readonly simpleGrid: boolean;
    readonly squareGrid: boolean;
    readonly pointsSelected: PointFeature[];
    readonly setPointsSelected: (points: PointFeature[]) => void;
    readonly setFilteredList1: (list: Feature<Geometry>[]) => void;
    readonly setFilteredList2: (list: Feature<Geometry>[]) => void;
    readonly setResultList: (list: Feature<Geometry>[]) => void;
    readonly setFirstListValidated: (validated: boolean) => void;
}